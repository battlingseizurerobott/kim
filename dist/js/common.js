$( document ).ready(function() {
  $("#slider-range").slider({
      range: true,
      min: 0,
      max: 500,
      values: [75, 300],
      slide: function(event, ui) {
        $("#filter-min").val(ui.values[0] + " грн.");
        $("#filter-max").val(ui.values[1] + " грн.");
      }
  });
  $("#filter-min").val("75 грн.");
  $("#filter-max").val("300 грн.");

  $('.product--img-small li a').click(function(){
    var href = $(this).prop('href');
    $('.product--img-small li').removeClass('active');
    $(this).parents('.product--img').find('.product--img-big img').prop('src',href);
    $(this).parents('li').addClass('active');
    return false;
  });

  var spinner = $( "#spinner" ).spinner({min: 1});
  $('.ui-spinner .ui-spinner-up .ui-button-icon-space').text('+');
  $('.ui-spinner .ui-spinner-down .ui-button-icon-space').text('-');


  var index = $('.index-slider').slick({
     arrows:false,
     slidesToShow: 1,
     slidesToScroll: 1,
   });

   var clients_slider_text = $('.clients-slider-img').slick({
      dots: false,
      infinite: true,
      swipeToSlide: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      speed: 1000,
      centerMode: true,
    });
    //
    // var clients_slider_img = $('.clients-slider-text').slick({
    //    slidesToShow: 1,
    //    slidesToScroll: 1,
    //    asNavFor: '.clients-slider-img',
    //    fade: true
    //  });

  $('.section-main-slider .slick-prev').on('click', function(e) {
     e.preventDefault();
     index.slick('slickPrev');
   })
  $('.section-main-slider .slick-next').on('click', function(e) {
     e.preventDefault();
     index.slick('slickNext');
   })

  $(".spinner").spinner({min: 1});
  $('.ui-spinner .ui-spinner-up .ui-button-icon-space').text('+');
  $('.ui-spinner .ui-spinner-down .ui-button-icon-space').text('-');

});
